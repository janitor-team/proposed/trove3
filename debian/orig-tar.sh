#!/bin/sh -e

# called by uscan with '--upstream-version' <version> <file>

TAR=$3

# clean up the upstream tarball
tar -x -z -f $TAR
mv $2/ trove-$2/
tar -c -z -f $TAR --exclude '*.jar' --exclude '*/javadocs/*' trove-$2/
rm -rf trove-$2/

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $TAR $origDir
  echo "moved $TAR to $origDir"
fi

